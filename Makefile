build-image:
	docker build -t extrator-texto:vvirtual .
remove-image:
	docker rmi extrator-texto:vvirtual
remove-cotainer: stop-cotainer
	docker rm extrator-texto
stop-cotainer:
	docker stop extrator-texto
run:
	docker run -ti -d --privileged=true  -p 5000:8080 --name extrator-texto  extrator-texto:vvirtual "/sbin/init"
start-cotainer:
	docker start extrator-texto
exec:
	docker exec -it extrator-texto /bin/bash

remove-all: remove-cotainer remove-image
clean: stop-cotainer remove-cotainer
clean_all: clean remove-image
