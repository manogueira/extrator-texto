from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from .models import Extrator
import uuid


@csrf_exempt 
def home(request):
    print(request.method)
    print(request.FILES)
    return HttpResponse('Olá Mundo!')

@csrf_exempt 
def upload(request):
    if request.method == 'POST' and request.FILES['file']:
        myfile = request.FILES['file']
        fs = FileSystemStorage(location=settings.MEDIA_ROOT) #defaults to   MEDIA_ROOT  
        filename = fs.save(str(uuid.uuid4()), myfile)
        file_url = fs.url(filename)
        f_ = '{}{}'.format(settings.MEDIA_ROOT,file_url)
        ext = Extrator(f_)
        out = ext.run()
        return JsonResponse(out)
    else:
         return HttpResponse('ERRO')