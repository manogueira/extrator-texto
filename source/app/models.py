
import requests
import subprocess
import uuid
import time
from django.conf import settings
import os



class Extrator(object):
    
    def __init__(self,_file):
        self.file = _file
        self.id = str(uuid.uuid4())
        self.tmp_path = settings.MEDIA_ROOT
        self.fExt = None
        self.jobs = 4 


    def getDocType(self,_file):
        response = {}
        response['status'] = False
        fin = open(_file,'rb')
        header = {'Accept': 'application/json'}
        rq = requests.put('http://localhost:9998/meta',data=fin,headers=header)
        fin.close()	
        if rq.status_code != 200:
            response['erro'] = 'Unsupported Media Type!'
            return response
        rq = rq.json()
        response['status'] = True
        response['Content-Type'] = rq['Content-Type']
        return response

    def tika(self,_file):	
        response = {}
        response['status'] = False
        fin = open(_file,'rb')
            
        try:
            rq = requests.put('http://localhost:9998/tika',data=fin)
            if(rq.status_code == 200):
                text = rq.content.decode('utf-8')
                text = text.split('\n')
                text = [t for t in text if t]			
                if text:
                    text = '\n'.join(text)
                    response['text'] = text
                    response['status'] = True
                    os.remove(_file)
                else:
                    response['step'] = 'tika'                        
        except requests.exceptions.RequestException as e:
            print(e)
        fin.close()
        
        return response


    def ocrmypdf(self,_file,_ext,_id):
        response = {}
        response['status'] = False
        new_file = '{}/{}.pdf'.format(self.tmp_path,_id)
        if self.fExt == 'pdf':		
            cmd = 'ocrmypdf -j {} {} {}'.format(self.jobs,_file,new_file)
        else:
            cmd = 'ocrmypdf -j {} --image-dpi 300  {} {}'.format(self.jobs,_file,new_file)
        try:
            run = subprocess.call(cmd,shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)	
            response['message'] = run
            response['status'] = True
            response['file'] = new_file
        except requests.exceptions.RequestException as e:
            response['step'] = 'ocrmypdf'
            response['message'] = e
            # os.remove(_file)
            os.remove(new_file)
        os.remove(_file)
        return response
        

        

    def run(self):
        #IP TIKA http://localhost:9998/tika
        #verificar o tipo de arquivo, caso seja pdf|doc, comecar com o tika, se nao, ocrmypdf
        #get file extension
        fExt = self.file.split('.')[-1]
        response = ''
        docType = self.getDocType(self.file)

        
        start_time = time.time()

        if not 'audio' in docType['Content-Type'] and not 'video' in docType['Content-Type']:
            print(docType)
            response = self.tika(self.file)
            if not response['status']:
                response = self.ocrmypdf(self.file,fExt,self.id)
                if response['status']:
                    n_file = response['file']
                    response = self.tika(n_file)
                    os.remove(n_file)

            elapsed_time = time.time() - start_time
            response['time'] = elapsed_time
            return response