FROM ubuntu:16.04
RUN apt-get update
RUN apt-get install -y build-essential git make wget vim iputils-ping openjdk-8-jre-headless software-properties-common python3-dev  python3-pip  tesseract-ocr curl ghostscript libexempi3 libffi6 pngquant qpdf  unpaper htop
RUN pip3 install --upgrade pip
RUN pip3 install --user ocrmypdf django django-cors-headers netifaces django-ipware requests

ENV PATH=$PATH:/root/.local/bin
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

RUN mkdir -p /extrator
COPY source /extrator
COPY source/django.service /etc/systemd/system/django.service
RUN systemctl enable django.service

COPY source/tika.service /etc/systemd/system/tika.service
COPY source/tika-server-1.20.jar /extrator
RUN systemctl enable tika.service

CMD [ "/sbin/init" ]
EXPOSE 8080
COPY source/start-web.sh /extrator
RUN chmod +x /extrator/start-web.sh


# ENTRYPOINT ["/extrator/start-web.sh"]



