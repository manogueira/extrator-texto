Construia a imagem do docker com seguinte comando do Makefile:
    make build-image
Assim que completar a construção da imagem, execute o docker com o seguinte comando:
    make run
Pronto, o docker com todas as ferramentas de extração de texto esta pronto e funcionando


Comando para testar o serviço

curl -X POST -F "file=@exemplos/bosh.pdf" localhost:5000
